import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    
        const firstFighterBarHealths = document.getElementById('left-fighter-indicator');
    const secondFighterBarHealths = document.getElementById('right-fighter-indicator');

    const fighterInfo = {
      currentHealth: 100,
      criticalAttack: false,
      StopTime: 0,
      block: false,
      EventArr: [],
    };

    const Player1 = { ...firstFighter, ...fighterInfo, healthIndicator: firstFighterBarHealths };
    const Player2 = { ...secondFighter, ...fighterInfo, healthIndicator: secondFighterBarHealths };

    function attackAction(attacker, defender) {
      const totalDamage = getDamage(attacker, defender);
      attacker.block = false;
      defender.currentHealth = defender.currentHealth - (totalDamage / defender.health) * 100;

      if (defender.currentHealth < 0) {
        defender.currentHealth = 0;
      }

      defender.healthIndicator.style.width = `${defender.currentHealth}%`;

      if (defender.currentHealth <= 0) {
        resolve(attacker);
      }
    }

    function criticalHandler(fighter) {
      const currentTime = Date.now();

      if (currentTime - fighter.StopTime < 10000) {
        return false;
      }

      if (!fighter.EventArr.includes(event.code)) {
        fighter.EventArr.push(event.code);
      }

      if (fighter.EventArr.length === 3) {
        fighter.criticalAttack = true;
        fighter.StopTime = currentTime;
        return true;
      }
    }

    function handlerKeyDown(event) {
      if (!event.repeat) {
        switch (event.code) {
          case controls.PlayerOneAttack: {
            attackAction(Player1, Player2);
            break;
          }

          case controls.PlayerTwoAttack: {
            attackAction(Player2, Player1);
            break;
          }

          case controls.PlayerOneBlock: {
            Player1.block = true;
            break;
          }

          case controls.PlayerTwoBlock: {
            Player2.block = true;
            break;
          }
        }

        if (controls.PlayerOneCriticalHitCombination.includes(event.code) && criticalHandler(Player1)) {
          attackAction(Player1, Player2);
        }

        if (controls.PlayerTwoCriticalHitCombination.includes(event.code) && criticalHandler(Player2)) {
          attackAction(Player2, Player1);
        }
      }
    }

    function handlerKeyUp(event) {
      switch (event.code) {
        case controls.PlayerOneBlock:
          Player1.block = false;
          break;
        case controls.PlayerTwoBlock:
          Player2.block = false;
          break;
      }

      if (Player1.EventArr.includes(event.code)) {
        Player1.EventArr.splice(Player1.EventArr.indexOf(event.code), 1);
      }

      if (Player2.EventArr.includes(event.code)) {
        Player2.EventArr.splice(Player2.EventArr.indexOf(event.code), 1);
      }
    }

    window.addEventListener('keydown', handlerKeyDown);
    window.addEventListener('keyup', handlerKeyUp);
  });
}

// !? done
export function getDamage(attacker, defender) {
  // return damage

  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  // return hit power

  const criticalHit = Math.random() + 1;
  return fighter.attack * criticalHit;
}

export function getBlockPower(fighter) {
  // return block power

  let dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
