import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const detailsContainer = createElement({ tagName: 'div', className: 'fighter-preview___details' });
    detailsContainer.insertAdjacentHTML(
      'beforeend',
      `<div class="block-info">
      <h2 class="name-player">${fighter.name}</h2>
      <div class="more">
      <p class="info-more ">❤️ Health: <span>${fighter.health}</span></p>
      <p class="info-more  ">	⚔️ Attack: <span>${fighter.attack}</span></p>
      <p class="info-more  ">🛡 Defense: <span>${fighter.defense}</span></p>
       </div>
       </div>
    `
    );

    fighterElement.append(fighterImage, detailsContainer);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
